{
    'name': 'Vendor management',
    'version': '12.0.1.0.0',
    'category': 'Extra Tools',
    'summary': 'Vendor Management System',
    'sequence': '10',
    'license': 'AGPL-3',
    'author': '',
    'maintainer': '',
    'website': '',
    'depends': ['sale','product'],
    'demo': [],
    'data': [
        "security/ir.model.access.csv",
        "views/vendor_view.xml"
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
