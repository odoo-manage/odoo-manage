import requests, json

from odoo import fields, models, api


headers = {"Content-Type": "application/json", "Accept": "application/json", "Catch-Control": "no-cache"}
class ProductTemplate(models.Model):
    _inherit = "product.template"

    def copy(self, default=None):
        copy = super(ProductTemplate, self).copy(default=default)
        print("copied")
        return copy



class Order(models.Model):
 _name = "vendor"


 code = fields.Char(string="Order Code", copy=False)
 itemName = fields.Text(string="Item Name")
 numbers = fields.Integer(string="Numbers")
 price = fields.Float(string="Price")
 total = fields.Float(string="Total")
 discount = fields.Integer(string="Discount")
