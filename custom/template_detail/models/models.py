# -*- coding: utf-8 -*-

from odoo import models, fields, api


class template_detail(models.Model):
    _name = 'template_detail.template_detail'
    _description = 'template_detail.template_detail'

    templateName = fields.Text(string="Template Name")
    price = fields.Float(string="Price")
    status = fields.Selection([('active','Active'),
                               ('deactive','Deactive')],
                              string="Status",)
    description = fields.Text(string="Description")
    template_image = fields.Image(string="Upload Template Image")
    url = fields.Char('URL', size=256, required=True)