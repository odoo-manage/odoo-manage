# -*- coding: utf-8 -*-

from odoo import models, fields, api


class template_master(models.Model):
    _name = 'template_master.template_master'
    _description = 'template_master.template_master'

    partner_id = fields.Many2one('res.partner', string='Vendor')
    product_id = fields.Many2one('product.template', string='Product')
    template_list = fields.Many2many("template_detail.template_detail", "template_detail_rel", "template_detail_id", "template_master_ìd",
                                  string="Choose template available or add new")


