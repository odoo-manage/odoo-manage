# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import psycopg2
import requests

from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError
from odoo.tools.safe_eval import json
from datetime import date


class StockImmediateTransferLine(models.TransientModel):
    _name = 'stock.immediate.transfer.line'
    _description = 'Immediate Transfer Line'

    immediate_transfer_id = fields.Many2one('stock.immediate.transfer', 'Immediate Transfer', required=True)
    picking_id = fields.Many2one('stock.picking', 'Transfer', required=True)
    to_immediate = fields.Boolean('To Process')

headers = {"Content-Type": "application/json", "Accept": "application/json", "Catch-Control": "no-cache"}
class GiaoHangTietKiem:
    def __init__(self, products, order):
        self.products = products,
        self.order = order,

class products:
    def __init__(self, name, weight, quantity, product_code):
        self.name = name,
        self.weight = weight,
        self.quantity = quantity,
        self.product_code = product_code,
class order:
    def __init__(self, client_id,type, pick_name,pick_address,pick_province,pick_district,pick_ward,pick_hamlet,pick_street,pick_tel,pick_email,pick_po_id,
                 name,address,province,district,ward,street,hamlet,tel,email,return_name,return_address,return_province,return_district,
                 return_ward,return_street,return_hamlet,return_tel,return_email,is_freeship,total_weight,value,transport,note,
                 tags,not_delivered_fee,not_delivered_option,is_delayed_time,pick_option,pick_time_slot,pick_work_shift,
                 pick_date,deliver_work_shift,deliver_time_slot,deliver_date,pick_address_id):
        self.client_id = client_id,
        self.type = type,
        self.pick_name = pick_name,
        self.pick_address = pick_address,
        self.pick_province = pick_province,
        self.pick_district = pick_district,
        self.pick_ward = pick_ward,
        self.pick_hamlet = pick_hamlet,
        self.pick_street = pick_street,
        self.pick_tel = pick_tel,
        self.pick_email = pick_email,
        self.province = province,
        self.district = district,
        self.ward = ward,
        self.hamlet = hamlet,
        self.is_freeship = is_freeship,
        self.pick_date = pick_date,
        self.name = name,
        self.note = note,
        self.value = value,
        self.transport = transport,
        self.pick_option = pick_option,
        self.address = address,
        self.street = street,
        self.tags = tags,
        self.tel = tel,
        self.email = email,
        self.return_name = return_name,
        self.return_address = return_address,
        self.return_province = return_province,
        self.return_district = return_district,
        self.pick_po_id = pick_po_id,
        self.return_street = return_street,
        self.return_ward = return_ward,
        self.return_hamlet = return_hamlet,
        self.return_tel = return_tel,
        self.return_email = return_email,
        self.total_weight = total_weight,
        self.not_delivered_fee = not_delivered_fee,
        self.not_delivered_option = not_delivered_option,
        self.is_delayed_time = is_delayed_time,
        self.pick_time_slot = pick_time_slot,
        self.pick_work_shift = pick_work_shift,
        self.deliver_work_shift = deliver_work_shift,
        self.deliver_time_slot = deliver_time_slot,
        self.deliver_date = deliver_date,
        self.pick_address_id = pick_address_id,


def update_vendor(label, id):
    """ update vendor name based on the vendor id """
    sql = """ UPDATE sale_order
                        SET ghtkid = %s
                        WHERE id = %s"""
    conn = None
    updated_rows = 0
    try:
        conn = psycopg2.connect(
            database="odoo15",
            user='graham',
            password='Nttdv@123',
            host='localhost',
            port='5432'
        )
        config = tools.config['url_config']
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (label, id))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return updated_rows


class StockImmediateTransfer(models.TransientModel):
    _name = 'stock.immediate.transfer'
    _description = 'Immediate Transfer'

    pick_ids = fields.Many2many('stock.picking', 'stock_picking_transfer_rel')
    show_transfers = fields.Boolean()
    immediate_transfer_line_ids = fields.One2many(
        'stock.immediate.transfer.line',
        'immediate_transfer_id',
        string="Immediate Transfer Lines")

    @api.model
    def default_get(self, fields):
        res = super().default_get(fields)
        if 'immediate_transfer_line_ids' in fields and res.get('pick_ids'):
            res['immediate_transfer_line_ids'] = [
                (0, 0, {'to_immediate': True, 'picking_id': pick_id})
                for pick_id in res['pick_ids'][0][2]
            ]
            # default_get returns x2m values as [(6, 0, ids)]
            # because of webclient limitations
        return res

    def process(self):
        pickings_to_do = self.env['stock.picking']
        pickings_not_to_do = self.env['stock.picking']
        config = tools.config['url_config']
        configGhtk = tools.config['ghtk_config']
        sellingPrices = list()
        conn = psycopg2.connect(
            database="odoo15",
            user='graham',
            password='Nttdv@123',
            host='localhost',
            port='5432'
        )
        get = "Get"
        update = "Update"
        cursor = conn.cursor()
        for line in self.immediate_transfer_line_ids:
            if line.to_immediate is True:
                pickings_to_do |= line.picking_id
            else:
                pickings_not_to_do |= line.picking_id

        for picking in pickings_to_do:
            # If still in draft => confirm and assign
            if picking.state == 'draft':
                picking.action_confirm()
                if picking.state != 'assigned':
                    picking.action_assign()
                    if picking.state != 'assigned':
                        raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
            picking.move_lines._set_quantities_to_reservation()


        pickings_to_validate = self.env.context.get('button_validate_picking_ids')
        # Xử lý để lưu trường số lượng tồn kho

        for product_id in picking.move_lines.product_id.ids:
            payloadDetail = {'productTmplId': str(self.id)}
            response = requests.get(config + get, params=payloadDetail, headers=headers)
            purchaseOrderSql = 'SELECT purchase_line_id FROM stock_move where product_id =' + str(product_id) + ' and state =' + "'assigned'" + ' order by create_date desc '
            saleOrderSql = 'SELECT sale_line_id FROM stock_move where product_id =' + str(product_id) + ' and state =' + "'assigned'" + ' order by create_date desc '
            cursor.execute(purchaseOrderSql)
            purchaseOrderExec = cursor.fetchone()
            cursor.execute(saleOrderSql)
            saleOrderExec = cursor.fetchone()
            productUomQtyPurchaseSql = 'SELECT product_uom_qty FROM stock_move where product_id =' + str(product_id) + ' and state =' + "'assigned'" + ' order by create_date desc '
            cursor.execute(productUomQtyPurchaseSql)
            productUomQtyPurchaseExec = cursor.fetchone()
            abc = str(''.join(map(str, productUomQtyPurchaseExec)))
            if str(''.join(map(str, purchaseOrderExec))) != "None":
                qtyAvailable = float(json.loads(response.text)["qtyAvailable"]) + float(abc)
            if str(''.join(map(str, saleOrderExec))) != "None":
                qtyAvailable = float(json.loads(response.text)["qtyAvailable"]) - float(abc)
            # if (json.loads(response.text)["qtyAvailable"]) == 0:
            #     if (picking.purchase_id.id is not None) and (picking.sale_id.id is False):
            #         qtyAvailable = picking.move_lines.product_id.qty_available + picking.move_lines.product_qty
            #     if (picking.sale_id.id is not None) and (picking.purchase_id.id is False):
            #         qtyAvailable = picking.move_lines.product_id.qty_available - picking.move_lines.product_qty
            # else:
            #     if (picking.purchase_id.id is not None) and (picking.sale_id.id is False):
            #         qtyAvailable = (json.loads(response.text)["qtyAvailable"]) + picking.move_lines.product_qty
            #     if (picking.sale_id.id is not None) and (picking.purchase_id.id is False):
            #         qtyAvailable = picking.move_lines.product_id.qty_available - picking.move_lines.product_qty

            payloadUpdate = {'id': json.loads(response.text)["id"]}
            payload = {
                "finalPrice": json.loads(response.text)["finalPrice"],
                "name": json.loads(response.text)["name"],
                "warehouseId": json.loads(response.text)["warehouseId"],
                "agentPointOdoo": json.loads(response.text)["agentPointOdoo"],
                "partnerPointOdoo": json.loads(response.text)["partnerPointOdoo"],
                "nameFilter": json.loads(response.text)["nameFilter"],
                "category": json.loads(response.text)["category"],
                "productCategoryOdoo": json.loads(response.text)["productCategoryOdoo"],
                "producerOdoo": json.loads(response.text)["producerOdoo"],
                "productProducerOdoo": json.loads(response.text)["productProducerOdoo"],
                "barcode": json.loads(response.text)["barcode"],
                "price": json.loads(response.text)["price"],
                "type": json.loads(response.text)["type"],
                "attributeOdoo": json.loads(response.text)["attributeOdoo"],
                "attributesOdoosLst": json.loads(response.text)["attributesOdoosLst"],
                "photo": json.loads(response.text)["photo"],
                "productImageOdoo": json.loads(response.text)["productImageOdoo"],
                "status": json.loads(response.text)["status"],
                "odooId": json.loads(response.text)["odooId"],
                "productTmplId": json.loads(response.text)["productTmplId"],
                "qtyAvailable": int(qtyAvailable),
            }
            r = requests.put(config + update, data=json.dumps(payload), headers=headers,
                             params=payloadUpdate)

            payload = {
                "products": [
                    {
                        "name": "bút",
                        "weight": 0.1,
                        "quantity": 1,
                        "product_code": 1241
                    },
                    {
                        "name": "tẩy",
                        "weight": 0.2,
                        "quantity": 1,
                        "product_code": 1254
                    }
                ],
                "order": {
                    "id": "a53",
                    "pick_name": "HCM-nội thành",
                    "pick_address": "590 CMT8 P.11",
                    "pick_province": "TP. Hồ Chí Minh",
                    "pick_district": "Quận 3",
                    "pick_ward": "Phường 1",
                    "pick_tel": "0911222337",
                    "tel": "0911222333",
                    "name": "GHTK - HCM - Noi Thanh",
                    "address": "123 nguyễn chí thanh",
                    "province": "TP. Hồ Chí Minh",
                    "district": "Quận 1",
                    "ward": "Phường Bến Nghé",
                    "hamlet": "Khác",
                    "is_freeship": "1",
                    "pick_date": "2016-09-30",
                    "pick_money": 47000,
                    "note": "Khối lượng tính cước tối đa: 1.00 kg",
                    "value": 3000000,
                    "transport": "fly",
                    "pick_option": "cod",
                    "tags": [
                        1,
                        7
                    ]
                }
            }

            headersNew = {"Content-Type": "application/json", "Accept": "application/json", "Catch-Control": "no-cache",
                          "Token": "c712D06685eb008aD89F6030440512EE7a93d2be"}
            r = requests.post(configGhtk, data=json.dumps(payload), headers=headersNew)
            if r.status_code == 200:
                label = json.loads(r.text)["order"]["label"]
                update_vendor(label, picking.sale_id.id)

        if pickings_to_validate:
            pickings_to_validate = self.env['stock.picking'].browse(pickings_to_validate)
            pickings_to_validate = pickings_to_validate - pickings_not_to_do
            return pickings_to_validate.with_context(skip_immediate=True).button_validate()
        return True



